"""
Errors and exceptions raised by the sum of squares package.
"""

class NotSupportedBySolver(Exception):
    """ The chosen solver cannot solve this problem. """


class SolverError(Exception):
    """ The solver failed to solve the problem """


class AlgebraicError(Exception):
    """ TODO """


class ConstraintWarning(UserWarning):
    """ A constraint was removed from the problem.  """

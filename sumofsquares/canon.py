""" Canonicalization of Problems """
import polymatrix as poly

from abc import abstractmethod
from dataclassabc import dataclassabc
from dataclasses import replace, field
from typing import Iterable
from typing_extensions import override

from polymatrix.expression.from_ import Expression
from polymatrix.expression.init import init_variable_expr
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import VariableIndex

from .abc import Problem, Solver, Constraint, Result
from .constraints import NonNegative, PositiveSemiDefinite, ExponentialCone
from .problems import SOSProblem, InternalSOSProblem
from .variable import OptSymbol, from_name as opt_variable_from_name
from .utils import partition


class Canonicalization(Problem):
    """
    Transform a problem into another problem.
    """
    def __post_init__(self):
        """ TODO: explain """
        if self.cost is None:
            object.__setattr__(self, "cost", self.problem.cost)

        if self.constraints is None:
            object.__setattr__(self, "constraints", self.problem.constraints)

    def __str__(self):
        s = f"Apply canonicalization procedure {self.__class__.__qualname__} to\n"
        return s + str(self.problem)

    @property
    @override
    def solver(self) -> Solver:
        return self.problem.solver

    @property
    @abstractmethod
    def problem(self) -> SOSProblem:
        """ The problem that will be canonicalized """

    @override
    def solve(self, state: ExpressionState, verbose: bool = False) -> tuple[ExpressionState, Result]:
        state, internal_prob = self.apply(state)
        return internal_prob.solve(state, verbose)

    @abstractmethod
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, InternalSOSProblem]:
        """
        Apply the canonicalization procedure.

        Override this method to write a transformation if it is stateful in the
        problem, and hence "needs to be performed later".
        """


@dataclassabc(frozen=True)
class PutinarPSatz(Canonicalization):
    """
    Apply Putinar's Positivstellensatz to replace the domain-restricted
    non-negativity constraints of a problem, with equivalent global
    non-negativity constraints by adding Langrange-multiplier-type polynomials.
    """

    problem: SOSProblem
    cost: Expression | None = None
    constraints: list[Expression] | None = None

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, InternalSOSProblem]:

        def need_positivstellensatz(c: Constraint) -> bool:
            return isinstance(c, NonNegative) and (c.domain is not None)

        constraints, to_process = partition(need_positivstellensatz, self.problem.constraints)
        constraints = tuple(constraints)
        to_process = tuple(to_process)
 
        # Variables that are involved in the problem
        variable_indices: set[VariableIndex] = set()

        # Evaluate expressions of constraints tha need to be processed
        # to gather all variables
        for c in to_process:
            state, pm = c.expression.apply(state)
            variable_indices |= pm.variables()

            for p in c.domain.polynomials:
                state, pm = p.apply(state)
                variable_indices |= pm.variables()

        # Get symbols of the variables
        to_process_symbols = set(state.get_symbol_from_variable_index(v)
                                 for v in variable_indices)

        # Separate between symbols of optimization variables and those of polynomial variables
        def is_opt(v):
            return isinstance(v, OptSymbol)

        polynomial_symbols, symbols = partition(is_opt, to_process_symbols)

        # Get variables from rest of the problem
        state, prob = replace(self.problem, constraints=constraints).apply(state)
        symbols = tuple(set(symbols) | set(prob.symbols))
        polynomial_symbols = tuple(set(polynomial_symbols) | set(prob.polynomial_symbols))

        # Vector of polynomial variables
        x = poly.v_stack((1,) + tuple(init_variable_expr(v, state.get_shape(v))
                                      for v in polynomial_symbols))

        # Create new constraints
        new_constraints: list[Constraint] = list(constraints)
        for i, constr in enumerate(to_process):
            # FIXME: create an extension poly_degree and opt_degree to get
            # the maximul degree of the polynomial and the degree of the
            # optimization variables respectively.
            constr_deg = constr.expression.linear_in(x).degree().max()

            # Add first term in the quadratic module
            d = constr_deg
            if d.read(state).scalar().constant() % 2 != 0:
                d += 1

            monomials = x.combinations(poly.arange(d + 1)) #.half_newton_polytope(x)
            coeffs = opt_variable_from_name(rf"\gamma_{i},z", shape=monomials.shape)

            multiplier = poly.give_name(coeffs.T @ monomials, rf"\gamma_{i},z")
            new_constr = constr.expression - multiplier

            # Add other terms of the quadratic module
            for j, domain_poly in enumerate(constr.domain.polynomials):
                # Make a multiplier polynomial for a domain polynomial of a constraint.
                # 
                # Since Putinar's PSatz does not explicity tell us the order of the
                # multiplier this function will make a multiplier, let's call it
                # :math:`\gamma`, such that :math:`\deg(\gamma * p) = \deg(c)`, wherein
                # :math:`p` is the domain polynomial and :math:`c` is the contraint
                # polynomial.

                domain_deg = domain_poly.linear_in(x).degree().max()

                # Degree of multiplier needs to be an integer
                d = constr_deg - domain_deg

                # FIXME: proper error here
                assert d.read(state).scalar().constant() >= 0, \
                        "Degree of domain polynomial is higher than constraint polynomial!"

                # Degree of multiplier must be even
                if d.read(state).scalar().constant() % 2 != 0:
                    d += 1

                # FIXME: need to check that there is not a \gamma_i,j already!
                # TODO: deterministically generate unique names

                # TODO: better documentation
                # Generate monomials of up to degree d and take only monomials
                # that are in half of the newton polytope
                monomials = x.combinations(poly.arange(d + 1)) # .half_newton_polytope(x)
                coeffs = opt_variable_from_name(rf"\gamma_{i},{j}", shape=monomials.shape)

                multiplier = poly.give_name(coeffs.T @ monomials, rf"\gamma_{i},{j}")

                # Subtract multiplier from expression and impose that it is also SOS
                new_constr -= multiplier * domain_poly
                new_constraints.append(NonNegative(multiplier))

            new_constraints.append(NonNegative(new_constr))
        return replace(self.problem, constraints=new_constraints).apply(state)


@dataclassabc(frozen=True)
class MaximizeLogDet(Canonicalization):
    # TODO: reconsider extension of polymatrix Expression objects to introduce
    # logdet expression. The current solution works but is not ideal, as it may
    # be a bit counterintuitive.
    """
    Create a new problem, whose cost function is the -logdet of the given
    problem's cost function.

    The problem
    
      maximize logdet(A)
    
    is equivalent to solving
    
      maximize      t
    
      subject to    [ A    L       ]
                    [ L.T  diag(L) ] >= 0
    
                    L lower triangular
                    t <= sum_i log(L[i,i])
    
    and the last constraint of the above is equivalent to
    
      t <= sum_i u[i]
      u_i <= log(L[i, i])  for all i
    
    And finally to get rid of the log the latter constraint one is
    equivalent to
    
      exp(u_i) <= L[i,i]

    so

      (u[i], 1, L[i,i]) in Exponential Cone for all i
    
    using the definition of the exponential cone of SCS.
    Hence we can replace the original problem with
    
      maximize      sum_i u[i]
    
      subject to    [ A    L       ]
                    [ L.T  diag(L) ] >= 0
    
                    L lower triangular
                    (u[i], 1, L[i,i]) in ExpCone for all i
    """
    problem: SOSProblem
    cost: Expression | None = None
    constraints: list[Expression] | None = None

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, InternalSOSProblem]:
        A = self.problem.cost

        # FIXME: should check for name clashes in state object
        # i.e. what if there is already a variable named u_logdet / L_logdet?
        # TODO: deterministically generate unique names

        n = A.shape[0]
        u = opt_variable_from_name('u_logdet', shape=(n, 1))

        m = poly.v_stack((n * (n + 1) / 2, 1))
        L = poly.lower_triangular(opt_variable_from_name('L_logdet', shape=m))

        Q = poly.concatenate(((A, L.T), (L, L.diag())))
        E = poly.h_stack((u, poly.ones((n, 1)), L.diag()))

        new_cost = - u.T.sum()

        new_constraints = tuple(self.problem.constraints) + (
            PositiveSemiDefinite(Q),
            ExponentialCone(E),
            NonNegative(u),
        )

        return replace(self.problem, cost=new_cost, constraints=new_constraints).apply(state)

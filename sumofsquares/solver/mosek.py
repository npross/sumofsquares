""" 
Solve sumofsquares problems using MOSEK
"""
from __future__ import annotations

import sys
import math
import mosek

import numpy as np

from collections import UserDict
from pathlib import Path
from typing import TYPE_CHECKING

from ..abc import Problem, SolverInfo, SolverStatus
from ..error import SolverError, NotSupportedBySolver
from ..variable import OptSymbol

if TYPE_CHECKING:
    from ..problems import ConicProblem


class MOSEKInfo(SolverInfo, UserDict):
    pass


def _streamprinter(text):
    # Helper function
    sys.stdout.write(text)
    sys.stdout.flush()


MOSEK_ENV: mosek.Env | None = None


def setup(license_file: Path | str | None = None):
    r"""
    Set up the MOSEK solver. If `license_file` is left unspecified, according to
    the official documentation MOSEK will search for a license file in one of
    the following directories depending on the operating system

    ::
        $HOME/mosek/mosek.lic
        %USERPROFILE%\mosek\mosek.lic
    """
    global MOSEK_ENV
    MOSEK_ENV = mosek.Env(licensefile=str(license_file))


def solve_cone(prob: ConicProblem, verbose: bool = False,
               *args, **kwargs) -> tuple[dict[OptSymbol, float], MOSEKInfo, SolverStatus]:
    r"""
    Solve a conic problem in the cone of SOS polynomials
    :math:`\mathbf{\Sigma}_d(x)` using MOSEK.
    """
    # TODO: implement proper interface to MOSEK
    if not any(prob.constraints[k] for k in ("b", "e", "e*", "p", "p*")):
        return solve_cone_via_cvxopt(prob, verbose, *args, **kwargs)

    raise NotImplementedError("Proper support of MOSEK is not implemented yet.")

    global MOSEK_ENV

    if not MOSEK_ENV:
        raise RuntimeError("You forgot to call `sumofsquares.solvers.mosek.setup(license)`!")

    if prob.constraints["z"]:
        raise NotImplementedError

    if prob.constraints["l"]:
        raise NotImplementedError

    if prob.constraints["b"]:
        raise NotImplementedError

    if prob.constraints["q"]:
        raise NotImplementedError

    if prob.constraints["s"]:
        raise NotImplementedError

    if prob.constraints["ep"]:
        raise NotImplementedError

    if prob.constraints["ep*"]:
        raise NotImplementedError

    if prob.constraints["p"]:
        raise NotImplementedError

    if prob.constraints["p*"]:
        raise NotImplementedError

    with MOSEK_ENV as env:
        with env.Task() as task:
            if verbose:
                task.set_Stream(mosek.streamtype.log, _streamprinter)

    status = SolverStatus.UNKNOWN
    return {}, MOSEKInfo(), status


def solve_cone_via_cvxopt(prob: ConicProblem, verbose: bool = False,
                          *args, **kwargs) -> tuple[dict[OptSymbol, float], MOSEKInfo, SolverStatus]:
    r"""
    Any `*args` and `**kwargs` other than `prob` and `verbose` are passed to the
    CVXOPT solver.
    """
    import cvxopt
    from .cvxopt import vectorize_matrix
    # CVXOPT can solve problems that have the (primal) form
    #
    #    minimize      .5 * x.T @ P @ x + q.T @ x
    #
    #    subject to    G @ x + s = h
    #                  A @ x = b
    #                  s >= 0
    #
    # Importantly, CVXOPT can only solve problems in the l, q and s cones.

    if prob.constraints["b"]:
        raise NotImplementedError("Automatic conversion of box constraints "
                                  "into linear constraints is not supported yet.")

    if prob.constraints["e"] or prob.constraints["e*"]:
        raise NotSupportedBySolver("CVXOPT cannot solve problems with the exponential cone.")

    if prob.constraints["p"] or prob.constraints["p*"]:
        raise NotSupportedBySolver("CVXOPT cannot solve problems with the power cone.")

    # Cost function
    q = cvxopt.matrix(prob.q) if prob.q is not None else None
    P = cvxopt.matrix(prob.P) if prob.P is not None else None

    # Constraint matrices
    A, b, G, h = None, None, None, None

    # Equality constraints
    A_rows, b_rows = [], []
    for (linear, constant) in prob.constraints["z"]:
        b_rows.append(vectorize_matrix(constant))
        A_rows.append(np.hstack(tuple(vectorize_matrix(m) for m in linear)))

    # pprint(f"{A_rows = }")
    # pprint(f"{b_rows = }")
        
    if A_rows:
        # multiplied by -1 because it is on the RHS
        b = cvxopt.matrix(np.vstack(b_rows))
        A = cvxopt.matrix(np.vstack(A_rows)) * -1

    # Inequality, second order and PSD constraints
    G_rows, h_rows = [], []

    for (linear, constant) in prob.constraints["l"]:
        h_rows.append(constant)
        G_rows.append(np.hstack(tuple(m for m in linear)))

    if prob.constraints["q"]:
        raise NotImplementedError("SOC constraints are not implemented yet.")

    for (linear, constant) in prob.constraints["s"]:
        # b is multiplied by -1 because it is on the RHS
        h_rows.append(vectorize_matrix(constant))
        G_rows.append(np.hstack(tuple(vectorize_matrix(m) for m in linear)))

    # pprint(f"{G_rows = }")
    # pprint(f"{h_rows = }")
        
    if G_rows:
        # multiplied by -1 because it is on the RHS
        h = cvxopt.matrix(np.vstack(h_rows))
        G = cvxopt.matrix(np.vstack(G_rows)) * -1

    # Format dims for CVXOPT
    dims = {
        "l": sum(prob.dims["l"]),
        "q": prob.dims["q"],
        "s": prob.dims["s"]
    }

    # Solve the problem
    # pprint({"A": A, "b": b, "G": G, "h": h})
    # pprint(dims)

    # print(A)
    # print(b)

    # print(G)
    # print(h)

    try:
        if prob.is_qp:
            assert P is not None, "Solving LP with QP solver! Something has gone wrong"
            info = cvxopt.solvers.qp(P=P, q=q, G=G, h=h, A=A, b=b, solver="mosek",
                                     dims=dims, *args, **kwargs)
        else:
            assert P is None, "Solving QP with LP solver! Something has gone very wrong"
            info = cvxopt.solvers.lp(c=q, G=G, h=h, A=A, b=b, solver="mosek",
                                     *args, **kwargs)

    except AssertionError as e:
        raise e

    except Exception as e:
        raise SolverError("CVXOpt can't solve this problem, "
                          "see previous exception for details on why.") from e

    match info["status"]:
        case "optimal":
            status = SolverStatus.OPTIMAL
        case "primal infeasible":
            status = SolverStatus.INFEASIBLE
        case "dual infeasible":
            status = SolverStatus.UNBOUNDED
        case _:
            status = SolverStatus.UNKNOWN

    if info['x'] is None:
        return {}, MOSEKInfo(info), status

    results, i = {}, 0
    for variable, shape in prob.variables.items():
        num_indices = math.prod(shape)
        values = np.array(info["x"][i:i+num_indices]).reshape(shape)
        if values.shape == (1, 1):
            values = values[0, 0]

        results[variable] = values
        i += num_indices 

    return results, MOSEKInfo(info), status

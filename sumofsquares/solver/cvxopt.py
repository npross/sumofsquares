"""
Solve sumofsquares problems using CVXOPT
"""
from __future__ import annotations

import cvxopt
import numpy as np
import scipy.sparse as sparse
import math

from numpy.typing import NDArray
from collections import UserDict
from typing import TYPE_CHECKING
from scipy.sparse import sparray
from pprint import pprint

from ..abc import SolverInfo, SolverStatus
from ..error import SolverError, NotSupportedBySolver
from ..variable import OptSymbol

if TYPE_CHECKING:
    from ..problems import ConicProblem


class CVXOPTInfo(SolverInfo, UserDict):
    """ Dictionary returned by CVXOPT. """

    def __getattr__(self, attr):
        # trick to make it behave like a dataclass
        key = " ".join(attr.split("_"))
        if key not in self.keys():
            raise KeyError(f"Key {key} was not found")
        return self[key]


def vectorize_matrix(m: NDArray | sparray) -> NDArray:
    r"""
    Vectorize a symmetric matrix for CVXOPT by stacking its columns. So,
    a :math:`n\times n` matrix becomes an :math:`n^2 \times 1` column vector.
    """
    # if m.shape[0] != m.shape[1]:
    #     raise ValueError("Matrix must be symmetric, and hence square!")
    if isinstance(m, sparray):
        m = m.toarray()

    return np.hstack(m.T.tolist()).reshape((-1, 1))


def solve_cone(prob: ConicProblem, verbose: bool = False,
               *args, **kwargs) -> tuple[dict[OptSymbol, NDArray | float], CVXOPTInfo, SolverStatus]:
    r"""
    Any `*args` and `**kwargs` other than `prob` and `verbose` are passed to the
    CVXOPT solver.
    """
    # CVXOPT can solve problems that have the (primal) form
    #
    #    minimize      .5 * x.T @ P @ x + q.T @ x
    #
    #    subject to    G @ x + s = h
    #                  A @ x = b
    #                  s >= 0
    #
    # Importantly, CVXOPT can only solve problems in the l, q and s cones.

    if prob.constraints["b"]:
        raise NotImplementedError("Automatic conversion of box constraints "
                                  "into linear constraints is not supported yet.")

    if prob.constraints["e"] or prob.constraints["e*"]:
        raise NotSupportedBySolver("CVXOPT cannot solve problems with the exponential cone.")

    if prob.constraints["p"] or prob.constraints["p*"]:
        raise NotSupportedBySolver("CVXOPT cannot solve problems with the power cone.")

    # Cost function
    q = cvxopt.matrix(prob.q) if prob.q is not None else None
    P = cvxopt.matrix(prob.P) if prob.P is not None else None

    # Constraint matrices
    A, b, G, h = None, None, None, None

    # Equality constraints
    A_rows, b_rows = [], []
    for (linear, constant) in prob.constraints["z"]:
        b_rows.append(vectorize_matrix(constant))
        A_rows.append(np.hstack(tuple(vectorize_matrix(m) for m in linear)))

    # pprint(f"{A_rows = }")
    # pprint(f"{b_rows = }")
        
    if A_rows:
        # multiplied by -1 because it is on the RHS
        b = cvxopt.matrix(np.vstack(b_rows)) * -1
        A = cvxopt.matrix(np.vstack(A_rows))

    # Inequality, second order and PSD constraints
    G_rows, h_rows = [], []

    for (linear, constant) in prob.constraints["l"]:
        h_rows.append(constant)
        G_rows.append(np.hstack(tuple(m for m in linear)))

    if prob.constraints["b"]:
        raise NotImplementedError

    if prob.constraints["q"]:
        raise NotImplementedError("SOC constraints are not implemented yet.")

    for (linear, constant) in prob.constraints["s"]:
        h_rows.append(vectorize_matrix(constant))
        G_rows.append(np.hstack(tuple(vectorize_matrix(m) for m in linear)))

    # pprint(f"{G_rows = }")
    # pprint(f"{h_rows = }")
        
    if G_rows:
        # multiplied by -1 because it is on the RHS
        h = cvxopt.matrix(np.vstack(h_rows))
        G = cvxopt.matrix(np.vstack(G_rows)) * -1


    # Format dims for CVXOPT
    dims = {
        "l": sum(prob.dims["l"]),
        "q": prob.dims["q"],
        "s": prob.dims["s"]
    }

    # Solve the problem
    # pprint({"A": A, "b": b, "G": G, "h": h})
    # pprint(dims)

    # print(A)
    # print(b)

    # print(G)
    # print(h)

    cvxopt.solvers.options["show_progress"] = verbose

    try:
        if prob.is_qp:
            assert P is not None, "Solving LP with QP solver! Something has gone wrong"
            info = cvxopt.solvers.coneqp(P=P, q=q, G=G, h=h, A=A, b=b,
                                         dims=dims, *args, **kwargs)
        else:
            assert P is None, "Solving QP with LP solver! Something has gone very wrong"
            info = cvxopt.solvers.conelp(c=q, G=G, h=h, A=A, b=b,
                                         *args, **kwargs)

    except AssertionError as e:
        raise e

    except Exception as e:
        raise SolverError("CVXOpt can't solve this problem, "
                          "see previous exception for details on why.") from e

    match info["status"]:
        case "optimal":
            status = SolverStatus.OPTIMAL
        case _:
            status = SolverStatus.UNKNOWN

    if info["x"] is None:
        return {}, CVXOPTInfo(info), status

    results, i = {}, 0
    for variable, shape in prob.variables.items():
        num_indices = math.prod(shape)
        values = np.array(info["x"][i:i+num_indices]).reshape(shape)
        if values.shape == (1, 1):
            values = values[0, 0]

        results[variable] = values
        i += num_indices 

    return results, CVXOPTInfo(info), status

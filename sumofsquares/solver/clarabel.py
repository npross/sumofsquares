"""
Solve sumofsquares problems using Clarabel.rs
"""
from __future__ import annotations

from typing import TYPE_CHECKING
from collections import UserDict

from numpy.typing import ArrayLike, NDArray
from scipy import sparse
import clarabel
import numpy as np
import math

from ..abc import OptSymbol, SolverInfo, SolverStatus
from ..error import SolverError

if TYPE_CHECKING:
    from ..problems import ConicProblem


class ClarabelInfo(SolverInfo, UserDict):
    def __getattr__(self, attr):
        # trick to make it behave like a dataclass
        key = " ".join(attr.split("_"))
        if key not in self.keys():
            raise KeyError(f"Key {key} was not found")
        return self[key]


def vec(m: ArrayLike | NDArray | sparse.sparray | sparse.spmatrix) -> sparse.csc_array:
    r""" Vectorise PSD matrix for clarabel by multiplying the off diagonal
    elements by :math:`\sqrt{2}` and taking the upper triangular part"""
    n = m.shape[0]
    if isinstance(m, np.ndarray):
        m = np.copy(m) * np.sqrt(2)
        m *= np.eye(n) / np.sqrt(2)
        return sparse.csc_array(m[np.triu_indices(n)].reshape((-1,1)))

    if isinstance(m, sparse.sparray | sparse.spmatrix):
        m *= np.sqrt(2)
        m *= sparse.eye(n) / np.sqrt(2)
        return sparse.triu(m).reshape((-1, 1)).tocsc()

    raise TypeError("Argument must be a numpy or a scipy.sparse array")


def mat(s: NDArray | sparse.sparray) -> NDArray:
    r"""
    Reconstruct a symmetric matrix from a vector that was created using
    :py:fn:`vec`. This scales the off-diagonal entries by :math:`1/\sqrt{2}`.
    """
    if isinstance(s, sparse.sparray | sparse.spmatrix):
        s = s.toarray()

    n = int((np.sqrt(8 * len(s) + 1) - 1) / 2)
    S = np.zeros((n, n))
    S[np.triu_indices(n)] = s / np.sqrt(2)
    S = S + S.T
    S[range(n), range(n)] /= np.sqrt(2)
    return S


def solve_cone(prob: ConicProblem, verbose: bool = False,
               *args, **kwargs) -> tuple[dict[OptSymbol | NDArray, float], ClarabelInfo, SolverInfo]:
    r"""
    Any `*args` and `**kwargs` other than `prob` and `verbose` are passed
    directly to the Clarabel solver call.
    """
    # Clarabel.rs expects the problem to be written in the form
    #
    #   minimize     .5 * x.T @ P @ x + q.T @ x
    #
    #   subj. to    Ax + s = b
    #               s in K
    #
    # where K is specified by a list containing clarabel.<CONE>T(<dim>)
    # For PSD matrices clarabel expects to receive the upper triangular part
    # of the matrix stacked column-wise.
    #
    # Clarabel expects matrices and vectors to be given with the following types
    #   P, A : Sparse CSC
    #   q, b : Dense numpy
    #
    n = prob.q.shape[0] if prob.q is not None else prob.P.shape[0]

    q = prob.q.reshape((-1,)) if prob.q is not None else np.zeros((n,))
    if prob.P is not None:
        P = sparse.triu(prob.P).tocsc()
    else:
        P = sparse.triu(sparse.csc_matrix((n, n))).tocsc()

    A_rows, b_rows, cones = [], [], []

    for (linear, constant), dim in zip(prob.constraints["z"], prob.dims["z"]):
        cones.append(clarabel.ZeroConeT(dim))
        b_rows.append(constant)
        A_rows.append(np.hstack(tuple(m for m in linear)))

    for (linear, constant), dim in zip(prob.constraints["l"], prob.dims["l"]):
        cones.append(clarabel.NonnegativeConeT(dim))
        b_rows.append(constant)
        A_rows.append(np.hstack(tuple(m for m in linear)))

    if prob.constraints["b"]:
        raise NotImplementedError

    if prob.constraints["q"]:
        raise NotImplementedError

    for (linear, constant), dim in zip(prob.constraints["s"], prob.dims["s"]):
        cones.append(clarabel.PSDTriangleConeT(dim))
        b_rows.append(vec(constant).toarray())
        A_rows.append(sparse.hstack(tuple(vec(m) for m in linear)))

    for (linear, constant) in prob.constraints["e"]:
        cones.append(clarabel.ExponentialConeT())
        for i in range(3):
            b_rows.append(constant[i])
            A_rows.append(sparse.hstack(tuple(m[i] for m in linear)))

    if prob.constraints["e*"]:
        raise NotImplementedError

    if prob.constraints["p"]:
        raise NotImplementedError

    if prob.constraints["p*"]:
        raise NotImplementedError


    # Negative sign because LHS
    A = sparse.vstack(A_rows).tocsc() * -1
    b = np.vstack(b_rows).reshape((-1,))

    # TODO: allow changing settings form outside, for other solvers it was enough to
    #  pass args / kwargs, but probably a better options would be to have
    #  a dictionary / dataclass internal to sumofsquares that contains settings for each solver
    #  something like dict[str, dict | Any] with str being the solver name and dict | Any for
    #  whatever that solver uses for configuration
    settings = clarabel.DefaultSettings()
    settings.verbose = verbose

    try:
        # print(f"{P.shape = }")
        # print(f"{q.shape = }")
        # print(f"{A.shape = }")
        # print(f"{b.shape = }")
        # print(cones)

        solver = clarabel.DefaultSolver(P=P, q=q, A=A, b=b, cones=cones, settings=settings)
        solution = solver.solve()

    # Clarabel uses a PyO3 for python bindings which wrap panic!() calls into exceptions that
    # according to the authors "should not be caught", hence it depends on BaseException instead
    # of being a normal exception
    except BaseException as e:
        raise SolverError("Clarabel.rs can't solve this problem, "
                          "see the previous exception for details on why.") from e

    status = SolverStatus.UNKNOWN
    match solution.status:
        case clarabel.SolverStatus.Solved:
            status = SolverStatus.OPTIMAL

        case clarabel.SolverStatus.PrimalInfeasible:
            status = SolverStatus.INFEASIBLE

        case clarabel.SolverStatus.DualInfeasible:
            status = SolverStatus.UNBOUNDED

        case clarabel.SolverStatus.AlmostSolved:
            status = SolverStatus.INACCURATE

    if status not in (SolverStatus.OPTIMAL, SolverStatus.INACCURATE):
        return {}, solution, status

    results, i = {}, 0
    for variable, shape in prob.variables.items():
        num_indices = math.prod(shape)
        values = np.array(solution.x[i:i + num_indices])

        if values.shape == (1, 1):
            values = values[0, 0]

        elif shape[0] == shape[1]:
            values = mat(values)

        else:
            values = values.reshape(shape)

        results[variable] = values
        i += num_indices

    return results, solution, status

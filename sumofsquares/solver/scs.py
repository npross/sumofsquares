""" 
Solve sumofsquares problems using SCS
"""
from __future__ import annotations

import scs
import math
import numpy as np

from collections import UserDict
from numpy.typing import NDArray
from scipy.sparse import csc_matrix, csc_array
from typing import TYPE_CHECKING

from ..abc import SolverInfo, SolverStatus
from ..error import SolverError
from ..variable import OptSymbol

if TYPE_CHECKING:
    from ..problems import ConicProblem


class SCSInfo(SolverInfo, UserDict):
    """ Dictionary returned by SCS. """

    def __getattr__(self, attr):
        key = " ".join(attr.split("_"))
        if key not in self.keys():
            raise KeyError(f"Key {key} was not found")
        return self[key]


def vec(S: NDArray | csc_array) -> NDArray:
    r"""
    Vectorize a symmetric matrix for SCS by storing only a half of the entries
    and multiplying the off-diagonal elements by :math:`\sqrt{2}`.
    """
    if isinstance(S, csc_array):
        S = S.toarray()

    n = S.shape[0]
    S = np.copy(S)
    S *= np.sqrt(2)
    S[range(n), range(n)] /= np.sqrt(2)
    return S[np.triu_indices(n)].reshape((-1,1))


def mat(s: NDArray) -> NDArray:
    r"""
    Reconstruct a symmetric matrix from a vector that was created using
    :py:fn:`vec`. This scales the off-diagonal entries by :math:`1/\sqrt{2}`. 
    """
    n = int((np.sqrt(8 * len(s) + 1) - 1) / 2)
    S = np.zeros((n, n))
    S[np.triu_indices(n)] = s / np.sqrt(2)
    S = S + S.T
    S[range(n), range(n)] /= np.sqrt(2)
    return S


def solve_cone(prob: ConicProblem, verbose: bool = False,
               *args, **kwargs) -> tuple[dict[OptSymbol | NDArray, float], SCSInfo, SolverInfo]:
    r"""
    Any `*args` and `**kwargs` other than `prob` and `verbose` are passed
    directly to the SCS solver call.
    """
    # SCS solves problems that have the following (primal) form
    #
    #    minimize      .5 * x.T @ P @ x + q.T @ x
    #
    #    subject to    Ax + s = b
    #                  s in K
    #
    #  where K is a product of cones (in the following order):
    #
    #   z    zero cone
    #   l    linear cone
    #   b    box cone
    #   q    second order cone
    #   s    positive semidefinite cone
    #   ep   exponential cone
    #   ed   dual exponential cone
    #   p    power cone
    #   -p   dual power cone
    #
    # In their documentation they call the linear coefficient c. Here we use q.

    P, q = None, None
    A_rows, b_rows = [], []

    if prob.q is not None:
        q = prob.q

    for (linear, constant) in prob.constraints["z"]:
        b_rows.append(constant)
        A_rows.append(np.hstack(tuple(m for m in linear)))

    for (linear, constant) in prob.constraints["l"]:
        b_rows.append(constant)
        A_rows.append(np.hstack(tuple(m for m in linear)))

    if prob.constraints["b"]:
        raise NotImplementedError

    if prob.constraints["q"]:
        raise NotImplementedError

    for (linear, constant) in prob.constraints["s"]:
        b_rows.append(vec(constant))
        A_rows.append(np.hstack(tuple(vec(m) for m in linear)))

    for (linear, constant) in prob.constraints["e"]:
        for i in range(3):
            b_rows.append(constant[i])
            A_rows.append(np.hstack(tuple(m[i] for m in linear)))

    if prob.constraints["e*"]:
        raise NotImplementedError

    if prob.constraints["p"]:
        raise NotImplementedError

    if prob.constraints["p*"]:
        raise NotImplementedError

    assert q is None or P is None or len(A_rows) > 0, "Problem is unconstrained! Something is very wrong"

    A = np.vstack(A_rows) * -1
    b = np.vstack(b_rows)

    data = {
        "P": csc_matrix(P) if P is not None else None,
        "c": q.reshape((-1,)),
        "A": csc_matrix(A),
        "b": b.reshape((-1,)),
    }

    cone = {
        "z": sum(prob.dims["z"]),
        "l": sum(prob.dims["l"]),
        "b": prob.dims["b"], # TODO: check
        "q": prob.dims["q"],
        "s": prob.dims["s"],
        "ep": sum(prob.dims["e"]),
        "ed": sum(prob.dims["e*"]),
        "p": prob.dims["p"] + list(-psize for psize in prob.dims["p*"]),
    }

    # Debugging
    # kwargs |= {
    #     "eps_abs": 1e-6, # default 1e-4
    #     "eps_rel": 1e-6, # default 1e-4
    #     "eps_infeas": 1e-12, # default 1e-7
    #     "rho_x": 1e-8, # defalt 1e-6
    #     "max_iters": int(1e6),
    # }

    try:
        solver = scs.SCS(data, cone, *args, verbose=verbose, **kwargs)
        # TODO: add mechanism to pass stuff for warm start, also store
        # ScsSolver object somewhere for reuse?
        sol = solver.solve()

    except Exception as e:
        raise SolverError("SCS can't solve this problem, "
                          "see previous exception for details on why.") from e

    # See exit flags in SCS documentation
    match sol["info"]["status_val"]:
        case 1:
            status = SolverStatus.OPTIMAL
        case 0:
            status = SolverStatus.INACCURATE
        case -1:
            status = SolverStatus.UNBOUNDED
        case -2:
            status = SolverStatus.INFEASIBLE
        case _:
            status = SolverStatus.UNKNOWN

    if sol["x"] is None:
        return {}, SCSInfo(sol["info"]), status

    results, i = {}, 0
    for variable, shape in prob.variables.items():
        num_indices = math.prod(shape)
        values = np.array(sol["x"][i:i+num_indices])

        if values.shape == (1, 1):
            values = values[0, 0]

        elif shape[0] == shape[1]:
            values = mat(values)

        else:
            values = values.reshape(shape)

        results[variable] = values
        i += num_indices

    return results, SCSInfo(sol["info"]), status
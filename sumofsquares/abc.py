""" Abstract base classes for Sum of Squares package """
from __future__ import annotations

from abc import ABC, abstractmethod
from enum import Enum, auto
from typing import Any, Generic, TypeVar

from sumofsquares.variable import OptSymbol


# ┏━┓┏━┓╻  ╻ ╻┏━╸┏━┓
# ┗━┓┃ ┃┃  ┃┏┛┣╸ ┣┳┛
# ┗━┛┗━┛┗━╸┗┛ ┗━╸╹┗╸

class Solver(Enum):
    """ Enumeration for the supported solvers. """
    CVXOPT = auto()
    CLARABEL = auto()
    MOSEK = auto()
    SCS = auto()


class SolverStatus(Enum):
    """ Enumeration for the supported states for the underlying conic solvers. """
    OPTIMAL = auto()
    """ Optimal solution found. """
    INACCURATE = auto()
    """ Solved but does not meet desired tolerance. """
    UNBOUNDED = auto()
    """ Primal unbounded or dual infeasible. """
    INFEASIBLE = auto()
    """ Primal infeasible or dual unbounded. """
    UNKNOWN = auto()
    """ SumOfSquares does not know, see SolverInfo struct. """


class SolverInfo(ABC):
    """ Type that information returned by a specific solver. """


# ┏━╸┏━┓┏┓╻┏━┓╺┳╸┏━┓┏━┓╻┏┓╻╺┳╸┏━┓
# ┃  ┃ ┃┃┗┫┗━┓ ┃ ┣┳┛┣━┫┃┃┗┫ ┃ ┗━┓
# ┗━╸┗━┛╹ ╹┗━┛ ╹ ╹┗╸╹ ╹╹╹ ╹ ╹ ┗━┛


class Set(ABC):
    r""" A set. In this context always a subset of :math:`\mathbf{R}^q` """


E = TypeVar("E")
class Constraint(ABC, Generic[E]):
    """ Optimization constraint. """
    @property
    @abstractmethod
    def expression(self) -> E:
        """ Expression under the constraint. """

    def __str__(self):
        return f"{self.__class__.__qualname__}({str(self.expression)})"


# ┏━┓┏━┓┏━┓┏┓ ╻  ┏━╸┏┳┓┏━┓
# ┣━┛┣┳┛┃ ┃┣┻┓┃  ┣╸ ┃┃┃┗━┓
# ╹  ╹┗╸┗━┛┗━┛┗━╸┗━╸╹ ╹┗━┛


class Result(ABC):
    """ Result of an optimization problem. """
    @abstractmethod
    def value_of(self, var: OptSymbol) -> float:
        """ Retrieve value of variable. """


class Problem(ABC):
    """ Optimization Problem. """

    # --- Properties ---

    @property
    @abstractmethod
    def cost(self) -> Any:
        """ Cost function of the problem. """
        # The type here is unspecified on purpose, see
        # programs module for why is it Any

    @property
    @abstractmethod
    def constraints(self) -> Any:
        """ Optimization constraints. """
        # The type here is unspecified on purpose, see
        # programs module for why is it Any

    @property
    @abstractmethod
    def solver(self) -> Solver:
        """ Which solver was / will be used to solve the problem. """

    # --- Methods ---

    @abstractmethod
    def solve(self, verbose: bool = False) -> Result:
        """ Solve the optimization problem. """

"""
Sum of Squares Package
======================

This is a package to solve sum-of-squares optimization problems.

Module Overview
---------------

Class diagram: Arrows denote inheritance, diamonds indicate composition.
::
                                                                                                                                       
   ┏━━sumofsquares package━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓                                   
   ┃                                                                                               ┃                                   
   ┃ ┌─.abc──────────────┐   ┌─.problems────────────────────────────────────────────────────┐      ┃                                   
   ┃ │ ┌──────────────┐  │   │                                                              │      ┃                                   
   ┃ │ │   Problem    │◀─┼───┼────────────┬───────────────────┬──────────────────┐          │      ┃                                   
   ┃ │ └──────────────┘  │   │            │                   │                  │          │      ┃                                   
   ┃ │         │         │   │  ┌───────────────────┐  ┌────────────┐  ┌──────────────────┐ │      ┃                                   
   ┃ │         ◇         │   │  │   ConicProblem    │  │ SOSProblem │  │InternalSOSProblem│ │      ┃                                   
   ┃ │ ┌──────────────┐  │   │  └───────────────────┘  └────────────┘  └──────────────────┘ │      ┃                                   
   ┃ │ │ Solver(Enum) │  │   │                                                              │      ┃                                   
   ┃ │ └──────────────┘  │   │                                                              │      ┃                                   
   ┃ │                   │   │                                                              │      ┃                                   
   ┃ │ ┌──────────────┐  │   │ ┌─────────────┐                                              │      ┃                                   
   ┃ │ │    Result    │◀─┼───┼─│ ConicResult │                                              │      ┃                                   
   ┃ │ └──────────────┘  │   │ └─────────────┘                                              │      ┃                                   
   ┃ │         │         │   └──────────────────────────────────────────────────────────────┘      ┃                                   
   ┃ │         │         │                                                                         ┃                                   
   ┃ │         ◇         │   ┌─.solvers.cvxopt──┐  ┌─.solvers.scs─────┐  ┌─.solvers.mosek───┐      ┃                                   
   ┃ │ ┌──────────────┐  │   │                  │  │                  │  │                  │      ┃                                   
   ┃ │ │  SolverInfo  │◀─┼───┼───────┬──────────┼──┼──────┬───────────┼──┼──────┐           │      ┃                                   
   ┃ │ └──────────────┘  │   │       │          │  │      │           │  │      │           │      ┃                                   
   ┃ │                   │   │ ┌──────────┐     │  │ ┌─────────┐      │  │ ┌─────────┐      │      ┃                                   
   ┃ │                   │   │ │CVXOptInfo│     │  │ │ SCSInfo │      │  │ │MosekInfo│      │      ┃                                   
   ┃ │                   │   │ └──────────┘     │  │ └─────────┘      │  │ └─────────┘      │      ┃                                   
   ┃ │                   │   └──────────────────┘  └──────────────────┘  └──────────────────┘      ┃                                   
   ┃ │                   │                                                                         ┃                                   
   ┃ │                   │   ┌─.constraints──────────────────────────────────────────────────────┐ ┃                                   
   ┃ │ ┌──────────────┐  │   │                                                                   │ ┃                                   
   ┃ │ │  Constraint  │◀─┼───┼───────────────────────────┬────────┬──────────┬─────────┐         │ ┃                                   
   ┃ │ └──────────────┘  │   │                           │        │          │         │         │ ┃                                   
   ┃ │ ┌──────────────┐  │   │ ┌───────────────────────┐ │  ┌───────────┐    │ ┌───────────────┐ │ ┃                                   
   ┃ │ │     Set      │◀─┼───┼─│ BasicSemialgebraicSet │ │  │EqualToZero│    │ │ExponentialCone│ │ ┃                                   
   ┃ │ └──────────────┘  │   │ └───────────────────────┘ │  └───────────┘    │ └───────────────┘ │ ┃                                   
   ┃ │                   │   │             ◇             │                   │                   │ ┃                                   
   ┃ │                   │   │             │       ┌───────────┐  ┌────────────────────┐         │ ┃                                   
   ┃ │                   │   │             └───────│NonNegative│  │PositiveSemiDefinite│         │ ┃                                   
   ┃ │                   │   │                     └───────────┘  └────────────────────┘         │ ┃                                   
   ┃ │                   │   │                                                                   │ ┃                                   
   ┃ │                   │   │                                                                   │ ┃                                   
   ┃ └───────────────────┘   └───────────────────────────────────────────────────────────────────┘ ┃                                   
   ┃                                                                                               ┃                                   
   ┃  ┌─.canon───────────────────────────────────────────────────┐  ┌─.variable─────────────────┐  ┃   ┏━━━polymatrix package━━━━━━━━━┓
   ┃  │                                                          │  │                           │  ┃   ┃                              ┃
   ┃  │ ┌────────────────────┐                                   │  │    ┌───────────┐          │  ┃   ┃  ┌────────┐                  ┃
   ┃  │ │  Canonicalization  │                                   │  │    │ OptSymbol │──────────┼──╋───╋─▶│ Symbol │                  ┃
   ┃  │ └────────────────────┘                                   │  │    └───────────┘          │  ┃   ┃  └────────┘                  ┃
   ┃  │            ▲                                             │  │                           │  ┃   ┃                              ┃
   ┃  │            ├────────────────────┐                        │  │                           │  ┃   ┃                              ┃
   ┃  │            │                    │                        │  │                           │  ┃   ┃                              ┃
   ┃  │    ┌───────────────┐      ┌───────────┐                  │  │                           │  ┃   ┃                              ┃
   ┃  │    │ PutinarPSatz  │      │  LogDet   │                  │  │                           │  ┃   ┃                              ┃
   ┃  │    └───────────────┘      └───────────┘                  │  │                           │  ┃   ┃                              ┃
   ┃  └──────────────────────────────────────────────────────────┘  └───────────────────────────┘  ┃   ┃                              ┃
   ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

"""
from typing import Iterable

import polymatrix as poly
from polymatrix.expression.expression import Expression
from polymatrix.expressionstate import ExpressionState

from .abc import Problem, Result, Set, Constraint, Solver
from .canon import Canonicalization, PutinarPSatz
from .constraints import BasicSemialgebraicSet, NonNegative
from .problems import SOSProblem
from .variable import (
        from_name as internal_from_name,
        from_names as internal_from_names)

# ╻ ╻┏━┓┏━┓╻┏━┓┏┓ ╻  ┏━╸┏━┓
# ┃┏┛┣━┫┣┳┛┃┣━┫┣┻┓┃  ┣╸ ┗━┓
# ┗┛ ╹ ╹╹┗╸╹╹ ╹┗━┛┗━╸┗━╸┗━┛

# export optimization variable constructors
from_name = internal_from_name
from_names = internal_from_names


# ┏━╸┏━┓┏┓╻┏━┓╺┳╸┏━┓┏━┓╻┏┓╻╺┳╸┏━┓
# ┃  ┃ ┃┃┗┫┗━┓ ┃ ┣┳┛┣━┫┃┃┗┫ ┃ ┗━┓
# ┗━╸┗━┛╹ ╹┗━┛ ╹ ╹┗╸╹ ╹╹╹ ╹ ╹ ┗━┛

def make_sos_constraint(expr: Expression, domain: Set | None = None) -> NonNegative:
    return NonNegative(expr, domain)


def make_set(*polynomials: Iterable[Expression]):
    return BasicSemialgebraicSet(polynomials)


# ┏━┓┏━┓┏━┓┏┓ ╻  ┏━╸┏┳┓┏━┓
# ┣━┛┣┳┛┃ ┃┣┻┓┃  ┣╸ ┃┃┃┗━┓
# ╹  ╹┗╸┗━┛┗━┛┗━╸┗━╸╹ ╹┗━┛

def make_problem(
        cost: Expression | None = None,
        constraints: Iterable[Constraint] = (),
        solver: Solver = Solver.CVXOPT,
        psatz: type[Canonicalization] | None = PutinarPSatz, 
    ) -> SOSProblem:
    """
    Create a sum-of-squares optimization problem.
    """
    if cost is None and not constraints:
        raise ValueError("Problem is empty, is has neither a cost nor any constraints!")
    elif cost is None:
        cost = poly.from_number(0)

    prob = SOSProblem(cost, constraints, solver)

    needs_psatz = any(c.domain is not None
        for c in (constr
                    for constr in constraints
                        if isinstance(constr, NonNegative)))

    if needs_psatz and psatz is not None:
        return psatz(SOSProblem(cost, constraints, solver))
    return prob


def minimize(cost, *args, **kwargs) -> SOSProblem:
    """ Alias for make_problem. """
    return make_problem(cost, *args, **kwargs)


def maximize(cost, *args, **kwargs) -> SOSProblem:
    """ Alias for make_problem(-cost, constraints, ...). """
    return make_problem(-cost, *args, **kwargs)


def solve_problem(*args, state: ExpressionState | None = None, verbose: bool = True, **kwargs) -> tuple[Problem, ExpressionState, Result]:
    """
    Solve a sum-of-squares optimization problem.
    This function is just a shorthand for
    .. py::

        prob = sos.make_problem(...)
        result = prob.solve(verbose)
    """
    prob = make_problem(*args, **kwargs)
    if state is None:
        state = poly.make_state()
    return (prob,) + prob.solve(verbose=verbose, state=state)


# ┏━┓┏━┓╻  ╻ ╻┏┓╻┏━┓┏┳┓╻┏━┓╻  ┏━┓
# ┣━┛┃ ┃┃  ┗┳┛┃┗┫┃ ┃┃┃┃┃┣━┫┃  ┗━┓
# ╹  ┗━┛┗━╸ ╹ ╹ ╹┗━┛╹ ╹╹╹ ╹┗━╸┗━┛


def polynomial_degree(p: Expression) -> Expression:
    """ Computes the maximal degree of p in the polynomial variables. """
    raise NotImplementedError


def decision_degree(p: Expression) -> Expression:
    """ Computes the maximal degree of p in the decision variables. """
    raise NotImplementedError


def make_cofactor(name: str, x: Expression, n: int | Expression, m: int | Expression):
    r"""
    Make a parametric cofactor of degree :math:`m - n`.
    
    This function creates a new a polynomial :math:`q \in \mathbf{R}(x)` in the
    polynomial variables :math:`x`, ,
    whose coefficients are decision variables given in a column vector named
    `name`. 

    Note that `p` must be a scalar polynomial in `x` and `x` itself must be a
    column vector of variables.

    Example:

    Suppose if you have two scalar polynomials :math:`f, p` and you want to
    crete a :math:`g` such that :math:`f * g` has the degree of :math:`p`,
    then
    .. py::

        x = poly.from_name("x", shape=(2,1))
        p = x.T @ x # x_1 ** 2 + x_2 ** 2
        f = poly.h_stack((1, 2)) @ x
        g = sos.make_cofactor("g", x, f, p.degree())

    """

    # TODO: what happens if p is a matrix? do this below?
    #
    # If :math:`P` is a matrix then the :math:`Q` is chosen such that
    # :math:`\deg(Q^T P) \leq \text{degree}`, where :math:`\deg` is understood
    # elementwise.

    if isinstance(degree, int):
        degree = poly.from_number(degree)

    # TODO: relace with polynomial_degree(p)
    q_degree = degree - p.degree()

    monoms = x.combinations(poly.arange(q_degree))
    coeffs = from_name(name, shape=(monoms.shape[1], monoms.shape[0]))

    return coeffs @ monoms

    


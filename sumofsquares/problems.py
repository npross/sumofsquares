""" 
Specific optimization problems that are related to Sum of Squares.

TODO: explanation for InternalSOSProblem
"""
from __future__ import annotations

import polymatrix as poly
import numpy as np

from dataclassabc import dataclassabc
from dataclasses import replace
from numpy.typing import NDArray
from scipy.sparse import csc_array
from typing import Any, Sequence
from typing_extensions import override
from warnings import warn

from polymatrix.expression.expression import Expression, VariableExpression
from polymatrix.expression.mixins.variableexprmixin import VariableExprMixin
from polymatrix.expression.init import init_variable_expr
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.index import MonomialIndex, VariableIndex
from polymatrix.symbol import Symbol

from .abc import Problem, Constraint, Solver, SolverStatus, Result
from .constraints import NonNegative, EqualToZero, PositiveSemiDefinite, ExponentialCone
from .error import ConstraintWarning
from .solver.cvxopt import solve_cone as cvxopt_solve_cone
from .solver.clarabel import solve_cone as clarabel_solve_cone
from .solver.mosek import solve_cone as mosek_solve_cone
from .solver.scs import solve_cone as scs_solve_cone
from .utils import partition
from .variable import OptSymbol


# ┏━╸┏━┓┏┓╻╻┏━╸   ┏━┓┏━┓┏━┓┏┓ ╻  ┏━╸┏┳┓
# ┃  ┃ ┃┃┗┫┃┃     ┣━┛┣┳┛┃ ┃┣┻┓┃  ┣╸ ┃┃┃
# ┗━╸┗━┛╹ ╹╹┗━╸   ╹  ╹┗╸┗━┛┗━┛┗━╸┗━╸╹ ╹


@dataclassabc(frozen=True)
class ConicProblem(Problem):
    """
    Conic program with linear or quadratic cost functions.
    All values stored in this class are numerical (numpy arrays).

    The problem has the following form
    ::

        minimize      .5 * x.T @ P @ x + q.T @ x
        
        subject to    CONSTRAINTS

    See docstring of :py:attr:`ConicProblem.constraints` for supported types of
    constraints.
    """
    # TODO: disallow NDArray
    P: csc_array | None
    """ Quadratic term of the cost function """

    q: NDArray | None
    """ Linear term of the cost function """

    is_qp: bool
    """ Is the problem quadratic? """

    dims: dict
    """ Dimensions of the cones """

    # TODO: Maybe do a dict[type, list[...]] instead?
    constraints: dict[str, list[tuple[tuple[NDArray | csc_array, ...], NDArray | csc_array]]]
    """ 
    Conic constraints are saved in a dictionary with the following form.
    For the key we will use the following names to refer to various 
    types of constraints
    ::

    NAME    LONG NAME                    MEANING
    ----    ---------------------------  ----------------------
       z    zero cone                    s = 0
       l    linear cone                  s >= 0
       b    box cone                     tl <= s <= tu
       q    second order cone            |s|_2 <= t
       s    positive semidefinite cone   s is PSD
       e    exponential cone             (x,y,z) in ExpCone
       e*   dual exponential cone        (u,v,w) in DualExpCone
       p    power cone
       p*   dual power cone

    Then the values are the coefficients of a LMI saved in a tuple `(linear
    terms,  constant term)`. The linear terms is again a tuple, with length
    equal the number of variables. The coefficients are left as matrices since
    each solver may expect then to be vectorized in a different way.

    See also solver.SOLVERNAME.solve_cone functions.
    """

    solver: Solver
    variables: dict[OptSymbol, tuple[int, int]]

    @property
    @override
    def cost(self) -> tuple[csc_array | None, NDArray | None]:
        return self.P, self.q

    @override
    def solve(self, verbose: bool = False) -> ConicResult:
        match self.solver:
            case Solver.CVXOPT:
                result, info, status = cvxopt_solve_cone(self, verbose)
            case Solver.CLARABEL:
                result, info, status = clarabel_solve_cone(self, verbose)
            case Solver.MOSEK:
                result, info, status = mosek_solve_cone(self, verbose)
            case Solver.SCS:
                result, info, status = scs_solve_cone(self, verbose)
            case _:
                raise NotImplementedError(f"Solver {self.solver} is not supported (yet).")

        return ConicResult(values=result, solver_info=info, solver_status=status)


@dataclassabc(frozen=True)
class ConicResult(Result):
    """ Result of a Conic Problem """
    values: dict[OptSymbol, float]
    solver_info: Any
    solver_status: SolverStatus

    @override
    def value_of(self, var: OptSymbol | VariableExpression) -> float:
        if isinstance(var, VariableExpression):
            if not isinstance(var.underlying, VariableExprMixin):
                # TODO: error message
                raise ValueError

            # Unwrap the expression
            symbol = var.underlying.symbol

            if symbol not in self.values:
                raise KeyError(f"There is no result for the variable {symbol}. "
                               f"Was the problem successfully solved?")

        return self.values[symbol]


# ┏━┓╻ ╻┏┳┓   ┏━┓┏━╸   ┏━┓┏━┓╻ ╻┏━┓┏━┓┏━╸┏━┓   ┏━┓┏━┓┏━┓┏━╸┏━┓┏━┓┏┳┓
# ┗━┓┃ ┃┃┃┃   ┃ ┃┣╸    ┗━┓┃┓┃┃ ┃┣━┫┣┳┛┣╸ ┗━┓   ┣━┛┣┳┛┃ ┃┃╺┓┣┳┛┣━┫┃┃┃
# ┗━┛┗━┛╹ ╹   ┗━┛╹     ┗━┛┗┻┛┗━┛╹ ╹╹┗╸┗━╸┗━┛   ╹  ╹┗╸┗━┛┗━┛╹┗╸╹ ╹╹ ╹


@dataclassabc(frozen=True)
class SOSProblem(Problem):
    """
    Generic sum of squares problem.
    This problem contains expression objects.
    """
    cost: Expression
    constraints: Sequence[Constraint[Expression]]
    solver: Solver

    # --- dunder methods ---

    def __str__(self):
        n = len(self.constraints)
        s = f"SumOfSquares Problem ({self.__class__.__qualname__}):\n\n  " \
            f"minimize  {self.cost}\n  " \
            f"subject to ({n} constraint{'s' if n > 1 else ''})\n"

        for i, c in enumerate(self.constraints):
            s += f"\tnr. {i}: {str(c)}\n"

        return s

    @override
    def solve(self, state: ExpressionState, verbose: bool = False) -> tuple[ExpressionState, Result]:
        state, internal_prob = self.apply(state)
        return internal_prob.solve(state, verbose)

    def apply(self, state: ExpressionState) -> tuple[ExpressionState, InternalSOSProblem]:
        """
        Convert to internal SOS problem by applying state to the expressions.

        **Technical Note:** The internal SOS problem may only have constraints
        that are affine in the optimization variables, hence, conversion of
        polynomial equality / non-negativity constraints are done here.
        Likewise the cost function must also be reduced to quadratic expression
        here.
        """
        variable_indices: set[VariableIndex] = set()
        constraints: list[Constraint] = []

        # Collect all variables that are involved in the problem
        state, cost = self.cost.apply(state)
        variable_indices |= cost.variables()

        for c in self.constraints:
            state, pm = c.expression.apply(state)
            variable_indices |= pm.variables()

        # Get symbols of the variables
        all_symbols = set(state.get_symbol_from_variable_index(v)
                          for v in variable_indices)

        # Separate between symbols of optimization variables and those of polynomial variables
        def is_opt(v):
            return isinstance(v, OptSymbol)

        polynomial_symbols, symbols = partition(is_opt, all_symbols)

        # evaluate generators
        symbols = tuple(symbols)
        polynomial_symbols = tuple(polynomial_symbols)

        x = poly.v_stack((1,) + tuple(
            init_variable_expr(v, state.get_shape(v))
            for v in polynomial_symbols))

        for i, c in enumerate(self.constraints):
            if isinstance(c, EqualToZero):
                state, deg = c.expression.linear_in(x).degree().max().apply(state)

                # Polynomial equality must be converted into coefficient
                # matching condition
                if deg.scalar().constant() > 1:
                    cnew = c.expression.linear_in(x)
                    constraints.append(replace(c, expression=cnew))

                # A normal (affine) equality
                else:
                    constraints.append(c)

            elif isinstance(c, NonNegative):
                if c.domain:
                    # See also class canon.PutinarPSatz
                    raise ValueError(f"Cannot convert non-negativity constraint nr. {i} "
                                     "to conic constraint. Domain restricted non-negativity "
                                     "must be preprocessed by using a Positivstellensatz!")

                if isinstance(c.expression.shape, tuple):
                    nrows, ncols = c.expression.shape

                # it is an expression
                else:
                    state, shape = c.expression.shape.apply(state)
                    nrows = shape.at(0, 0).constant()
                    ncols = shape.at(1, 0).constant()

                # Row wise interpretation, we need to know the shape
                if ncols > 1:
                    raise ValueError(f"Cannot convert non-negativity constraint {str(c)} (nr {i}) "
                                     "because it has more than one column. If you want a "
                                     "matrix to be PSD use PositiveSemiDefinite, if you want "
                                     "to have a vector in the non-negative orthant of R^n the "
                                     "interpretation is row-wise, not column wise (transpose "
                                     "your vector)")

                for row in range(nrows):
                    row_expr = c.expression[row, :]
                    state, deg = row_expr.linear_monomials(x).degree().max().apply(state)
                    d = deg.scalar().constant()

                    # Polynomial non-negativity constraints is converted to PSD
                    # constraint of SOS quadratic form
                    if d > 1:
                        if d % 2 != 0:
                            # raise ValueError("Cannot convert expression inside non-negativity "
                            #                  f"constraint {str(c)} (nr. {i}) into quadratic form for PSD condition "
                            #                  f"because it has degree {deg.scalar().constant()}, which "
                            #                  "is not even.")
                            warn("Size of SDP increased because to convert "
                                 f"constraint {str(c)} (nr. {i}) into quadratic form for PSD condition "
                                 f"its degree (= {deg.scalar().constant()}) was increased by one to make it even.",
                                 ConstraintWarning)

                            monomials = x.combinations(tuple(range(d + 2)))
                            cnew = row_expr.quadratic_in(x, monomials).symmetric()

                        else:
                            cnew = row_expr.quadratic_in(x).symmetric()

                        constraints.append(PositiveSemiDefinite(cnew))

                    # A normal (affine) constraint
                    else:
                        constraints.append(replace(c, expression=row_expr))

            elif isinstance(c, PositiveSemiDefinite):
                state, pm = c.expression.cache().apply(state)
                nrows, ncols = pm.shape
                if nrows != ncols:
                    raise ValueError(f"PSD constraint cannot contain non-square matrix of shape ({nrows, ncols})!")

                # PSD constraint can be passed as-is
                constraints.append(c)

            elif isinstance(c, ExponentialCone):
                state, pm = c.expression.shape.apply(state)

                if pm.at(1, 0).constant() != 3:
                    raise ValueError("Conic constraint must be a row vector [x, y, z] ",
                                     "or for multiple constraints it must be an n x 3 "
                                     f"matrix! Given expression has wrong shape {pm.shape}.")

                constraints.append(c)

            else:
                raise NotImplementedError(f"Cannot process constraint of type {type(c)} (yet).")

        # Convert Expressions into PolyMatrix objects
        pm_constraints: list[Constraint[PolyMatrixMixin]] = []
        for i, c in enumerate(constraints):
            state, pm = c.expression.apply(state)
            constr = poly.to_affine(pm)

            # Remove constraints that have disappeared
            # There is only one "variable" in the affine expression
            if len(constr.slices.keys()) == 1:
                # That variable is actually not a variable
                if MonomialIndex.constant() in constr.slices.keys():
                    # constraint has disappeared
                    warn(f"Constraint on {str(c.expression)} was removed.", ConstraintWarning)
                    continue
                    # FIXME: need to check for each type of constraint
                    # that it is ok to remove constant value. Example of
                    # a problematic edge case is NonNegative(a - a - 5)
                    # where a is an optimization variable.

            pm_constraints.append(replace(c, expression=constr))

        return state, InternalSOSProblem(cost, tuple(pm_constraints),
                                         symbols, polynomial_symbols,
                                         self.solver)


# FIXME: I hate this name, and the fact that this class needs to exist
@dataclassabc(frozen=True)
class InternalSOSProblem(Problem):
    """
    This is a class for internal use only. It is a SOS problem that contains
    polymatrix objects.

    It exists only because expressions are lazily evaluated, so if we want to
    perform transformations on problems (see canon module), we may need to
    apply the state to the expressions to convert the expressions to polymatrix
    objects.
    """
    cost: PolyMatrixMixin
    constraints: Sequence[Constraint[PolyMatrixMixin]]
    symbols: Sequence[OptSymbol]
    polynomial_symbols: Sequence[Symbol]
    solver: Solver

    def __str__(self):
        n = len(self.constraints)
        s = f"Internal SumOfSquares Problem ({self.__class__.__qualname__}):\n\n" \
            f"minimize  {self.cost}\n" \
            f"subject to ({n} constraint{'s' if n > 1 else ''})\n"

        for i, c in enumerate(self.constraints):
            s += f"nr. {i}:\n{str(c)}\n\n"

        return s

    def to_conic_problem(self, state: ExpressionState, verbose: bool = False) -> ConicProblem:
        """ Convert the SOS problem into a Conic program. """
        cost = poly.to_affine(self.cost)
        if cost.degree > 2:
            raise ValueError("This package can solve at most quadratic conic programs, "
                             f"but the given problem has degree {cost.degree} > 2.")

        is_qp = (cost.degree == 2)

        # Sizes
        # See docstring of ConicProblem.constraints
        dims: dict[str, list[int]] = {
            "z": [], "l": [], "b": [], "q": [], "s": [],
            "e": [], "e*": [], "p": [], "p*": [],
        }

        # See docstring of ConicProblem.constraints
        constraints: dict[str, list[tuple[tuple[NDArray, ...], NDArray]]] = {
            "z": [], "l": [], "b": [], "q": [], "s": [],
            "e": [], "e*": [], "p": [], "p*": [],
        }

        # indices of variables in the optimization problem, sorted
        variable_indices = sum(sorted(tuple(state.get_indices_as_variable_index(v))
                                      for v in self.symbols), ())

        # cost linear term
        cost_coeffs = dict(cost.affine_coefficients_by_degrees(variable_indices))
        q = cost_coeffs.get(1)
        if q is not None:
            q = q.reshape((-1, 1)).toarray()

        elif not is_qp:
            # Allow LP without cost function
            q = np.zeros((len(variable_indices), 1))

        # cost quadratic term
        if is_qp:
            n = len(variable_indices)
            P = csc_array((n, n))
            # This works because of how monomial indices are sorted
            # see also polymatrix.index.MonomialIndex.__lt__
            P[np.triu_indices(n)] = cost_coeffs[2]

            # Make symmetric
            P = .5 * (P + P.T)

        else:
            P = None

        for c in self.constraints:
            constr = c.expression
            nrows, ncols = constr.shape

            if constr.degree > 1:
                # If this error occurs and it is not the user's fault, there is
                # a bug in SOSProblem.apply
                raise ValueError("To convert to conic constraints must be linear or affine "
                                 f"but {str(c.expression)} has degree {constr.degree} "
                                 "(There is a product of decision variables).")

            # Get constant and linear terms
            constant = constr.affine_coefficient(MonomialIndex.constant()).toarray()
            linear = tuple(constr.affine_coefficient(v).toarray() for v in variable_indices)

            if isinstance(c, EqualToZero):
                dims["z"].append(nrows)
                constraints["z"].append((linear, constant))

            elif isinstance(c, NonNegative):
                dims["l"].append(nrows)
                constraints["l"].append((linear, constant))

            elif isinstance(c, PositiveSemiDefinite):
                dims["s"].append(nrows)
                constraints["s"].append((linear, constant))

            elif isinstance(c, ExponentialCone):
                # Interpret row-wise
                dims["e"].append(nrows)
                for i in range(nrows):
                    constraints["e"].append((
                        tuple(m[i, :] for m in linear),
                        constant[i, :]))

            else:
                raise NotImplementedError(f"Cannot convert constraint of type {type(c)} "
                                          "into a conic constraint (yet).")

        if all(len(cl) == 0 for cl in constraints.values()):
            raise ValueError("Optimization problem is unconstrained!")

        if verbose:
            logstr = ""

            logstr += "  Decision variables:   " + ", ".join(self.symbols) + "\n"
            logstr += "  Polynomial variables: " + ", ".join(self.polynomial_symbols) + "\n"

            if P is not None:
                logstr += f"  {P.shape = }\n"

            if q is not None:
                logstr += f"  {q.shape = }\n"

            logstr += "  Cones have dimensions:\n"
            for ctype, d in dims.items():
                if d:
                    logstr += f"    {ctype}: {sum(d)} = sum{tuple(d)}\n"

            print("Conic problem: \n" + logstr)

        return ConicProblem(P=P, q=q, constraints=constraints,
                            dims=dims, is_qp=is_qp,
                            solver=self.solver,
                            variables={v: state.get_shape(v)
                                       for v in self.symbols
                                       })

    @override
    def solve(self, state: ExpressionState, verbose: bool = False) -> tuple[ExpressionState, Result]:
        return state, self.to_conic_problem(state=state, verbose=verbose).solve(verbose)

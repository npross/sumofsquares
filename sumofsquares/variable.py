"""
This module is an extension of the polymatrix package that adds a new type of
variable, a decision variable, to formulate optimization problems. Speficically,
this extends the following parts of polymatrix:

    - polymatrix.variable, by adding a new OptVariable type
    - polymatrix.expression, by adding an OptVariableMixin, so that there can
      be expression with underlying objects that are OptVariables.
"""

from __future__ import annotations

from typing import Iterable

from polymatrix.expression.expression import Expression, VariableExpression, init_variable_expression
from polymatrix.expression.from_ import from_any
from polymatrix.expression.init import init_variable_expr
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.symbol import Symbol


class OptSymbol(Symbol):
    """ Symbol for an optimization (decision) variable. """


def from_name(name: str, shape: tuple[int, int] | ExpressionBaseMixin = (1, 1)) -> VariableExpression:
    """ Construct an optimization variable. """
    if isinstance(shape, Expression):
        shape = shape.underlying

    elif isinstance(shape, tuple):
        nrows, ncols = shape
        if (not isinstance(nrows, int)) or (not isinstance(ncols, int)):
            shape = from_any(((nrows,), (ncols,))).underlying

    return init_variable_expression(underlying=init_variable_expr(OptSymbol(name), shape))


def from_names(names: str, shape: tuple[int, int]  | ExpressionBaseMixin = (1, 1)) -> Iterable[VariableExpression]:
    """ Construct one or multiple variables from comma separated a list of names. """
    for name in names.split(","):
        yield from_name(name.strip(), shape)

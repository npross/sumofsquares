from dataclasses import dataclass
from dataclassabc import dataclassabc
from typing_extensions import override
from typing import Generic, TypeVar

from polymatrix.expression.expression import Expression
from polymatrix.polymatrix.mixins import PolyMatrixMixin

from .abc import Set, Constraint


# Type variable for expression or polymatrix
E = TypeVar("E", bound=Expression | PolyMatrixMixin)


# ┏━┓┏━╸╺┳╸┏━┓
# ┗━┓┣╸  ┃ ┗━┓
# ┗━┛┗━╸ ╹ ┗━┛

@dataclass(frozen=True)
class BasicSemialgebraicSet(Set, Generic[E]):
    r"""
    A set that is described by the intersection of the positive loci of a
    finite number of polynomials (under some conditions). Roughly speaking this
    is a set of the form
    ..math::

        \mathbf{K} = \{
            x : g_i(x) \geq 0,
                g_i \in \mathbf{R}(x),
                x \in \mathbf{R}^q,
                i = 1, \ldots, N
        \}
        \subseteq \mathbf{R}^q.

    This tuple stores the :math:`g_i(x)`.
    """
    polynomials: tuple[E, ...]

    def __str__(self):
        polynomials = ', '.join(f"{p} >= 0" for p in self.polynomials)
        return f"{self.__class__.__qualname__}({polynomials})"


# ┏━╸┏━┓┏┓╻┏━┓╺┳╸┏━┓┏━┓╻┏┓╻╺┳╸┏━┓
# ┃  ┃ ┃┃┗┫┗━┓ ┃ ┣┳┛┣━┫┃┃┗┫ ┃ ┗━┓
# ┗━╸┗━┛╹ ╹┗━┛ ╹ ╹┗╸╹ ╹╹╹ ╹ ╹ ┗━┛


@dataclassabc(frozen=True)
class EqualToZero(Constraint[E]):
    """ Constrain an expression to be equal to zero. """
    expression: E

    @override
    def __str__(self):
        return f"{str(self.expression)} = 0"


@dataclassabc(frozen=True)
class NonNegative(Constraint[E]):
    """
    Constrain a scalar expression to be non-negative in a certain domain. If
    the domain in set to `None` then it is interpreted as being non-negative
    everywhere.

    If the expression is a column vector, the interpretation is row-wise. In
    other words each row must be non-negative on the domain (or everywhere if
    not specified). In other words the vector is in the non-negative orthant of
    R^n.

    **Note:** In this package, non-negativitiy of non-affine constraint will
    always eventually be replaced by the sufficient condition that the
    expression can be written as a sum-of-squares, hence this constraint can
    also be understood as `expression` being SOS.
    """

    expression: E 
    domain: BasicSemialgebraicSet[E] | None = None

    @override
    def __str__(self):
        s = f"{str(self.expression)} >= 0"
        if self.domain:
            s += f"\n\t\t over {self.domain}"
        return s


@dataclassabc(frozen=True)
class PositiveSemiDefinite(Constraint[E]):
    """ Constrain a matrix to be positive semidefinite. """
    expression: E

    @override
    def __str__(self):
        return f"{str(self.expression)} is PSD"


@dataclassabc(frozen=True)
class ExponentialCone(Constraint[E]):
    r"""
    This constraint expects a 3d row vector :math:`(x, y, z)`, and imposes that
    the point is inside of the exponential cone.

    The expression may also be a :math:`n\times 2` matrix. In that case the
    constraint is understood row-wise, or in other words, each row is taken as
    a single point that must be inside of the exponential cone.
    """
    expression: E

    @override
    def __str__(self):
        return f"{str(self.expression)} in ExpCone"


# TODO: Create missing classes below and add support in
#  - SOSProblem.apply
#  - InternalSOSProblem.to_conic_problem
#  - solver.SOLVERNAME.solve_cone (for each solver)


# @dataclassabc(frozen=True)
# class SecondOrderCone(Constraint[E]):
#     """ Constrain an expression to be in the second order cone. """
#     expression: E
#     t: E # FIXME: review this


# @dataclassabc(frozen=True)
# class DualExponentialCone(Constraint[E]):
#     ...

# @dataclassabc(frozen=True)
# class PowerCone(Constraint[E]):
#     ...


# @dataclassabc(frozen=True)
# class DualPowerCone(Constraint[E]):
#     ...
